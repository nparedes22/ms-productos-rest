package com.acl.producto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsProductosRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsProductosRestApplication.class, args);
	}

}
