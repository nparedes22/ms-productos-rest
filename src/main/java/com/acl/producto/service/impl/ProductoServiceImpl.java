package com.acl.producto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.acl.producto.model.dao.ProductoDao;
import com.acl.producto.model.entity.Producto;
import com.acl.producto.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService{
	
	@Autowired
	private ProductoDao productoDao;
	
	@Override
	@Transactional(readOnly = true)
	public Producto findById(Long id) {
		return productoDao.findById(id).orElseGet(null);
	}

	@Override
	public List<Producto> findAll() {
		// TODO Auto-generated method stub
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	public Boolean create(Producto producto) {
		Boolean exito=true;
		try {
			productoDao.save(producto);
		}
		catch(Exception e) {
			return false;
		}
		catch(Throwable t) {
			return false;
		}
		
		return exito;
	}

	@Override
	public Boolean update(Producto producto) {
		Boolean exito=true;
		try {
			productoDao.save(producto);
		}
		catch(Exception e) {
			return false;
		}
		catch(Throwable t) {
			return false;
		}
		return exito;
	}

	@Override
	public Boolean delete(Long id) {
		Boolean exito=true;
		try {
			productoDao.deleteById(id);
		}
		catch(Exception e) {
			return false;
		}
		catch(Throwable t) {
			return false;
		}
		return exito;
	}

}
